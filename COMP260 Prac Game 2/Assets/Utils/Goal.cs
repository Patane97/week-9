﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {

    public delegate void ScoreGoalHandler(int player);
    public ScoreGoalHandler scoreGoalEvent;
    public int player;

    public AudioClip scoreClip;
    private AudioSource audio;

    void Start ()
    {
        audio = GetComponent<AudioSource>();
    }

    void OnTriggerEnter (Collider collider)
    {
        //Plays score sound.
        audio.PlayOneShot(scoreClip);

        //Resets puck to original position.
        PuckControl puck = collider.gameObject.GetComponent<PuckControl>();
        puck.ResetPosition();

        if (scoreGoalEvent != null)
        {
            scoreGoalEvent(player);
        }
    }
}
